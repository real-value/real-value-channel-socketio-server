let debug = require('debug')('ChannelSocketIOServer')

function ChannelFactory (options) {

    var IO = require('socket.io')

    let { server } = options

    let outBuffer = []
    let inBuffer = {}

    var io = IO(server);

    let peerCount = 0

    function startSending(socket){
        while(outBuffer.length>0){
            debug(`Emit Server`)
            socket.emit('Channel',outBuffer.shift())
        }
        if(peerCount>=1){
            setTimeout(()=>{startSending(socket)},500)
        }else {
            debug('Stop sending')
        }
    }

    io.on('connection', function(socket){
        debug('a user connected')
        peerCount++

        if(peerCount===1) startSending(socket)
        
        socket.on('Channel',(msg)=>{
            debug('Server Received')
            if(inBuffer[msg.channel]===undefined){
                inBuffer[msg.channel]=[]
            }
            inBuffer[msg.channel].push(msg.value)
        })

        socket.on('disconnect', ()=>{
            peerCount--
            debug('user disconnected')
        })
        
    })

    return (name)=> {
        return {
            enqueue: x => {
                debug(`enqueue ${name}`)
                outBuffer.push({channel: name, value: x})
            },
            length: () => inBuffer[name]!==undefined ? inBuffer[name].length : 0,
            dequeue: () => {
                debug(`dequeue ${name} buffer length:${inBuffer[name].length}`)
                return inBuffer[name].shift()
            }
        }
    }
}

module.exports = ChannelFactory
