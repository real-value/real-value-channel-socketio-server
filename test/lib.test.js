import { describe } from 'riteway'
const debug = require('debug')('test')

let {ChannelFactory: ChannelSocketIOServer } = require('../index.js')
import {ChannelFactory as ChannelSocketIOClient} from 'real-value-channel-socketio-client'

describe('Channel', async (assert) => {

  var app = require('express')();
  var server = require('http').Server(app);
  server.listen(9000,()=>{debug('Listening')});

  let channelServer = ChannelSocketIOServer({server})('production')

  let channelClient = ChannelSocketIOClient({url: "http://127.0.0.1:9000"})('production')

  channelServer.enqueue({ fromServer: '1'})
  channelClient.enqueue({ fromClient: '2'})

  await new Promise((resolve)=>{ setTimeout(resolve,1000)})

  let clientItem = null
  if(channelServer.length()>0){
    clientItem= channelServer.dequeue()
  }

  let serverItem = null
  if(channelClient.length()>0){
    serverItem = channelClient.dequeue()
  }

  assert({
    given: 'Content sent',
    should: 'content be received',
    actual: `${clientItem.fromClient} ${serverItem.fromServer}`,
    expected: '2 1'
  })

  channelClient.close()
  debug('Close Server')
  server.close(()=>{
    process.exit(0)
  })
  
})
