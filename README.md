# Real-Value-Channel-SocketIO-Server Library    

An implemententation of a server communication channel using socket io.

## How to use

The goal of this functionality is to support the following type of expression
```
model.from([1,2,3]).toChannel('production)
model.fromChannel('production').log()
```

## Install

```
npm install real-value-channel-socketio-server
```


